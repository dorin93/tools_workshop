Project Title
=============

One Paragraph of the project description goes here.

|

.. contents::

|


Getting Started
---------------

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

**Requirements**:

What things you need to install to use the repository.

* Python 3.6
* Tensorflow 1.14
* Python packages as listed in requirements.txt.

**Instructions:**

Recommended way to install the requirements.

.. code:: text

    pip install -r requirements.txt

|

Repository Structure
--------------
Explain about the project file system structure

.. code:: text

    * src - project source code:
        * data_analysis
        * preprocess
        * evaluation
    * test - all the automatic test are here
    * tools - the entry points to run the repo capabilities:
        * run_preprocess.py - run the process on the data
        * evaluate.py - create evaluation report

    * requirements.txt - python requirements
    * bitbucket-pipelines.yml - CI/CD configuration tool
    * bitbucket-pipelines_requirements.txt - requirements for CI/CD tool
    * pylintrc - configuration file for pylint (static code analysis tool)

|

Running the tests
-----------------

Explain how to run the automated tests for this system.

.. code:: text

    py.test tests

|

Authors
-------
* **Razor 770** - *office@razor-labs.com*
