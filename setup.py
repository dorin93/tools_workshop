import sys

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand

with open("README.rst", "r") as fh:
    long_description = fh.read()


class PyTest(TestCommand):
    user_options = [("pytest-args=", "a", "Arguments to pass to pytest")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = []

    def run_tests(self):
        # import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)


requirements = ["pytest==4.6.6",
                "pytest-mock==1.10.0",
                "numpy==1.19.5"
                ]

setup(name="tools_workshop",
      version="0.0.2",
      author="Dorin Zohar",
      author_email="dorin@razor-labs.com",
      description="clean code 2 - tools - workshop",
      long_description=long_description,
      long_description_content_type="text/markdown",
      url="https://bitbucket.org/dorin93/tools_workshop",
      packages=find_packages(),
      tests_require=["pytest", "pytest-mock"],
      cmdclass={"test": PyTest},
      install_requires=requirements)
