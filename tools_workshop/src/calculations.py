import numpy as np


def calculate_sqrt(n):
    return np.sqrt(n)


def calculate_quadrilateral_area(pt1, pt2, pt3, pt4):
    x1, y1 = pt1
    x2, y2 = pt2
    x3, y3 = pt3
    x4, y4 = pt4

    area = abs(0.5 * ((x1*y2 + x2*y3 + x3*y4 + x4*y1) - (x2*y1 + x3*y2 + x4*y3 + x1*y4)))

    return area


if __name__ == '__main__':
    import argparse
    import yaml

    parser = argparse.ArgumentParser(description='Process input')
    parser.add_argument('--yaml_path', help='Path to configuration'
                                            ' yaml file', type=str)
    args = parser.parse_args()

    with open(args.yaml_path, 'r') as yaml_file:
        points_dict = yaml.load(yaml_file, Loader=yaml.SafeLoader)
        print(points_dict)

    area = calculate_quadrilateral_area(points_dict['point1'], points_dict['point2'],
                                        points_dict['point3'], points_dict['point4'])

    print(area)
