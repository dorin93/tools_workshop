from tools_workshop.src.calculations import calculate_quadrilateral_area


class TestCalculations(object):

    def test_calculate_quadrilateral_area(self):
        """ Test example function  """
        point1 = [0, 0]
        point2 = [5, 10]
        point3 = [10, 12]
        point4 = [5, 5]

        expected_value = 25

        actual_value = calculate_quadrilateral_area(point1, point2, point3, point4)

        assert actual_value == expected_value
