Tools
=====

This directory goal is to expose a CLI (command line interface) to the project capabilities
(which are under src directory).

|

.. contents::

|

runner_1.py
---------------
Description of the tool.

Usage Example

.. code:: text

    option1: runner_1.py param1 param2
    option2: runner_1.py config.yaml

